package i

import "time"

var Delimiter = "/"

type Mailbox struct {
	Messages   []*Message
	Name       string
	Owner      *User
	Subscribed bool
}

func (mBox *Mailbox) GetFlags() []string {
	flagsMap := make(map[string]bool)
	for _, msg := range mBox.Messages {
		for _, f := range msg.Flags {
			if !flagsMap[f] {
				flagsMap[f] = true
			}
		}
	}

	var flags []string
	for f := range flagsMap {
		flags = append(flags, f)
	}
	return flags
}

func (mBox *Mailbox) SetSubscribed(subscribed bool) {
	mBox.Subscribed = subscribed
}

func (mBox *Mailbox) GetNextUid() uint32 {
	var uid uint32
	for _, msg := range mBox.Messages {
		if msg.Uid > uid {
			uid = msg.Uid
		}
	}
	uid++
	return uid
}

func (mBox *Mailbox) CreateMessage(flags []string, date time.Time, body string) {
	if date.IsZero() {
		date = time.Now()
	}

	mBox.Messages = append(mBox.Messages, &Message{
		Uid:   mBox.GetNextUid(),
		Date:  date,
		Size:  uint32(len([]byte(body))),
		Flags: flags,
		Body:  body,
	})
}

func (mBox *Mailbox) GetMessages() (messages []*Message) {
	for _, msg := range mBox.Messages {
		messages = append(messages, msg)
	}
	return
}