package c

import (
	"bitbucket.org/infinyte/mail-core/i"
	"sync"
	"time"
	"log"
)

var (
	backendChannel = make(chan ChannelMessage)
	mutex          sync.Mutex
)

const MsgResponse = "Response"

const MsgUserLookup = "UserLookup"
const MsgDomainLookup = "DomainLookup"

const MsgAuthenticationAttempt = "AuthenticationAttempt"

func LookupUser(username string, duration time.Duration) (*i.User) {
	channelMessage := NewMessage()
	channelMessage.Identifier = MsgUserLookup
	channelMessage.SetParameter("Username", username)

	Send(channelMessage)

	channelResponse := WaitFor(channelMessage.Uid, duration*time.Second)

	userObj := channelResponse.GetParameter("User")

	if userObj != nil {
		iUser := userObj.(*i.User)
		return iUser
	}
	return nil
}

func AuthenticateUser(user *i.User, password string, duration time.Duration) (bool) {
	channelMessage := NewMessage()
	channelMessage.Identifier = MsgAuthenticationAttempt
	channelMessage.SetParameter("User", user)
	channelMessage.SetParameter("Password", password)

	Send(channelMessage)

	channelResponse := WaitFor(channelMessage.Uid, duration*time.Second)

	answer := channelResponse.GetParameter("LoginState")

	if answer == nil {
		answer = false
	}

	return answer.(bool)
}

func WaitFor(uid string, d time.Duration) (channelMessage *ChannelMessage) {
	defer mutex.Unlock()
	mutex.Lock()

	for channelMessage == nil {
		select {
		case msg := <-backendChannel:
			if msg.Uid == uid {
				channelMessage = &msg
				return
			}
		case <-time.After(d):
			log.Println("Timeout")
			return NewMessage()
		}
	}

	return
}

func Send(channelMessage *ChannelMessage) {
	backendChannel <- *channelMessage
}

func GetChannel() (*chan ChannelMessage) {
	return &backendChannel
}
