package s

import (
	"bitbucket.org/infinyte/mail-core/l"
	"bitbucket.org/infinyte/mail-core/s/s/b"
	"github.com/emersion/go-smtp"
	"github.com/emersion/go-sasl"
)

var (
	bridgeBackend = new(b.BackendBridge)
)

type SmtpServer struct {
	backend  *b.BackendBridge
	Host     string
	Port     string
	Insecure bool
	server   *smtp.Server
}

func (smp *SmtpServer) Start() {

	smp.backend = bridgeBackend
	smp.server = smtp.NewServer(smp.backend)
	smp.server.AllowInsecureAuth = smp.Insecure
	smp.server.Addr = smp.Host + ":" + smp.Port

	smp.server.EnableAuth(sasl.Login, func(conn *smtp.Conn) sasl.Server {
		return sasl.NewLoginServer(func(username, password string) (err error) {
			user, err := smp.backend.Login(username, password)

			if user != nil {
				conn.SetUser(user)
			}

			return
		})
	})

	if err := smp.server.ListenAndServe(); err != nil {
		l.Fatal(err)
	}
}

func (smp *SmtpServer) Stop() {
	smp.server.Close()
}
