package main

import (
	"bitbucket.org/infinyte/mail-core/s/i"
	"bitbucket.org/infinyte/mail-core/s/s"
	"path/filepath"
	"os"
	"log"
	"io/ioutil"
	"bitbucket.org/infinyte/mail-core/p"
	"bitbucket.org/infinyte/mail-core/s/c"
)

var (
	imapServer = new(i.ImapServer)
	smtpServer = new(s.SmtpServer)
)

func main() {

	ex, _ := os.Executable()
	cwd := filepath.Dir(ex)

	files, err := ioutil.ReadDir(cwd + "/Plugins")
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		p.LoadPlugin(cwd + "/Plugins" + "/" + f.Name())
	}

	imapServer.Insecure = true
	imapServer.Host = "127.0.0.1"
	imapServer.Port = "1143"
	go imapServer.Start()

	smtpServer.Insecure = true
	smtpServer.Host = "127.0.0.1"
	smtpServer.Port = "1025"
	go smtpServer.Start()

	dev := p.NewDevBackend()
	go dev.ListenAndServe(c.GetChannel())

	select {}
}