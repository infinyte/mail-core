package p

import (
	"os"
	goPlugin "plugin"
	"errors"
	"log"
)

var (
	ErrorFileNotExistent   = errors.New("plugin does not exist")
	ErrorLoadingPlugin     = errors.New("error while loading plugin from disk")
	ErrorMissingPluginVar  = errors.New("plugin does not export itself in 'Plugin' var")
	ErrorInvalidPluginType = errors.New("plugin does not match 'Plugin' interface")
)

func LoadPlugin(pluginPath string) (plugin Plugin, err error) {

	// Is there really a file ?

	if _, osErr := os.Stat(pluginPath); os.IsNotExist(osErr) {
		err = ErrorFileNotExistent
		return
	}

	// Let's see if we can load this file as a valid go plugin

	plug, loadErr := goPlugin.Open(pluginPath)
	if loadErr != nil {
		err = ErrorLoadingPlugin
		return
	}

	// Check for the necessary "Plugin" Variable

	pluginGreeter, lookupErr := plug.Lookup("Plugin")
	if lookupErr != nil {
		err = ErrorMissingPluginVar
		return
	}

	// Apply InfinytePlugin scheme to third-party code

	plugin, ok := pluginGreeter.(Plugin)
	if !ok {
		err = ErrorInvalidPluginType
		plugin = nil
		return
	}

	// Notify the plugin that it has been loaded

	log.Println("Loading Plugin " + plugin.Name() + " v" + plugin.Version() + "...")
	plugin.OnLoad()
	log.Println("Loaded Plugin " + plugin.Name())
	return
}
