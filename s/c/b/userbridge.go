package b

import (
	"github.com/emersion/go-imap/backend"
	"bitbucket.org/infinyte/mail-core/i"
	"io"
	"strings"
	"log"
	"time"
	"net"
	"github.com/emersion/go-smtp"
	"bitbucket.org/infinyte/mail-core/s/c"
	"io/ioutil"
)

type UserBridge struct {
	theRealSlimUser *i.User
}

func (u *UserBridge) GetBridgedUser() (*i.User) {
	return u.theRealSlimUser
}

func (u *UserBridge) Username() string {
	return u.GetBridgedUser().Username
}

func (u *UserBridge) ListMailboxes(subscribed bool) (mailboxes []backend.Mailbox, err error) {
	for _, mailbox := range u.GetBridgedUser().ListMailboxes(subscribed) {
		mailboxes = append(mailboxes, NewMailboxBridge(mailbox, u))
	}
	return
}

func (u *UserBridge) GetMailbox(name string) (mailbox backend.Mailbox, err error) {
	mBox, err := u.GetBridgedUser().GetMailbox(name)
	mailbox = NewMailboxBridge(mBox, u)
	return
}

func (u *UserBridge) CreateMailbox(name string) error {
	return u.GetBridgedUser().CreateMailbox(name)
}

func (u *UserBridge) DeleteMailbox(name string) error {
	return u.GetBridgedUser().DeleteMailbox(name)
}

func (u *UserBridge) RenameMailbox(existingName, newName string) error {
	return u.GetBridgedUser().RenameMailbox(existingName, newName)
}

func (u *UserBridge) Logout() error {
	return nil
}

func NewUserBridge(user *i.User) (userBridge *UserBridge) {
	return &UserBridge{
		user,
	}
}

func (u *UserBridge) Send(from string, to []string, r io.Reader) error {
	for _, recipient := range to {

		if strings.Contains(recipient, "@") {

			if user := c.LookupUser(recipient, 5); user != nil {

				mailbox, err := user.GetMailbox("INBOX")
				b, err := ioutil.ReadAll(r)

				if err == nil {
					msg := string(b)
					mailbox.CreateMessage(nil, time.Now(), msg)
				} else {
					log.Println("aawwww crap")
				}

			} else {

				if u.GetBridgedUser().Anonymous {
					continue
				}

				mxDomain := strings.Split(recipient, "@")[1]
				mxServers, err := net.LookupMX(mxDomain)

				if err != nil {
					log.Println("Could not resolve " + mxDomain)
					continue
				} else {
					for _, mxServer := range mxServers {
						smtpClient, err := smtp.Dial(mxServer.Host + ":25")

						if err != nil {
							log.Println("MX " + mxServer.Host + " not reachable!")
							// TODO: Notify User about delay & requeue
							continue
						}

						err = smtpClient.Hello("localhost")
						if err != nil {
							log.Println("MX " + mxServer.Host + " refused EHLO!")
							continue
						}
						err = smtpClient.Mail(from)
						if err != nil {
							log.Println("MX " + mxServer.Host + " refused FROM!")
							continue
						}
						err = smtpClient.Verify(recipient)
						if err != nil {
							log.Println("Recipient not known to MX " + mxServer.Host + " (or VRFY was rejected)")
						}
						err = smtpClient.Rcpt(recipient)
						if err != nil {
							log.Println("MX " + mxServer.Host + " refused RCPT!")
							continue
						}
						data, err := smtpClient.Data()
						if err != nil {
							log.Println("MX " + mxServer.Host + " refused DATA!")
							continue
						}
						io.Copy(data, r)
						data.Close()
						smtpClient.Quit()
						break
					}
				}
			}

		} else {
			log.Println("Recipient is not known " + recipient)
		}

		log.Println("Sent")
	}

	return nil
}
