package i

import (
	"github.com/emersion/go-imap/server"
	"bitbucket.org/infinyte/mail-core/l"
	"bitbucket.org/infinyte/mail-core/s/i/b"
)

var (
	bridgeBackend = new(b.BackendBridge)
)

type ImapServer struct {
	backend  *b.BackendBridge
	Host     string
	Port     string
	Insecure bool
	server   *server.Server
}

func (imp *ImapServer) Start() {

	imp.backend = bridgeBackend
	imp.server = server.New(imp.backend)
	imp.server.AllowInsecureAuth = imp.Insecure
	imp.server.Addr = imp.Host + ":" + imp.Port

	if err := imp.server.ListenAndServe(); err != nil {
		l.Fatal(err)
	}
}

func (imp *ImapServer) Stop() {
	err := imp.server.Close()

	if err != nil {
		l.Fatal(err)
	}
}
