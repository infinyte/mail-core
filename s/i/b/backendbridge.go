package b

import (
	"github.com/emersion/go-imap/backend"
	"bitbucket.org/infinyte/mail-core/s/c"
	"log"
	"sync"
	"bitbucket.org/infinyte/mail-core/s/c/b"
)

type BackendBridge struct {
	backend.Backend
	Mutex sync.Mutex
}

func (bck BackendBridge) Login(username, password string) (user backend.User, err error) {
	defer bck.Mutex.Unlock()
	bck.Mutex.Lock()

	err = backend.ErrInvalidCredentials

	userObject := c.LookupUser(username, 5)

	if userObject != nil {

		loginState := c.AuthenticateUser(userObject, password, 5)

		if loginState {
			log.Println("Login succeeded for " + username)
			bridgedUser := b.NewUserBridge(userObject)
			user = bridgedUser
			err = nil
		} else {
			log.Println("Login failed for " + username)
			err = backend.ErrInvalidCredentials
		}
	}
	return
}
