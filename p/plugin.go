package p

type Plugin interface {
	/**
	Executed after the plugin was loaded
	 */
	OnLoad()

	OnEnable()

	OnDisable()

	Name() string

	/**
	Return Version of the plugin
	 */
	Version() string
}
