package c

import "math/rand"

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

type ChannelMessage struct {
	Uid        string
	Identifier string
	Parameters map[string]interface{}
}

/**
Set message parameter by name
 */
func (cMsg *ChannelMessage) SetParameter(name string, parameter interface{}) {
	cMsg.Parameters[name] = parameter
}

func (cMsg *ChannelMessage) GetParameter(name string) (parameter interface{}) {
	parameter, _ = cMsg.Parameters[name]
	return
}

func generateUid() string {
	b := make([]byte, 32)
	for i := range b {
		b[i] = letterBytes[rand.Int63()%int64(len(letterBytes))]
	}
	return string(b)
}

func NewMessage() (*ChannelMessage) {
	channelMessage := new(ChannelMessage)
	channelMessage.Uid = generateUid()
	channelMessage.Parameters = map[string]interface{}{}
	return channelMessage
}

func NewMessageResponse(message *ChannelMessage) (*ChannelMessage) {
	channelMessage := new(ChannelMessage)
	channelMessage.Identifier = MsgResponse
	channelMessage.Uid = message.Uid
	channelMessage.Parameters = map[string]interface{}{}
	return channelMessage
}
