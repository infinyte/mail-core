package i

import "errors"

var (
	ErrNoSuchMailbox = errors.New("no such mailbox")
	ErrMailboxExists = errors.New("mailbox already exists")
	ErrDeleteProtect = errors.New("cannot delete protected box")
	ErrCreateMailbox = errors.New("cannot create mailbox")
)

var (
	ProtectedMailboxes = []string{"INBOX",}
)

type User struct {
	Authenticated bool
	Anonymous     bool
	Username      string
	Mailboxes     map[string]*Mailbox
}

func (usr *User) ListMailboxes(subscribed bool) (mailboxes []*Mailbox) {
	for _, mailbox := range usr.Mailboxes {
		if subscribed && !mailbox.Subscribed {
			continue
		}

		mailboxes = append(mailboxes, mailbox)
	}
	return
}

func (usr *User) GetMailbox(name string) (mailbox *Mailbox, err error) {
	mailbox, exists := usr.Mailboxes[name]
	if !exists {
		err = ErrNoSuchMailbox
	}
	return
}

func (usr *User) ExistsMailbox(name string) (exists bool) {
	_, exists = usr.Mailboxes[name]
	return
}

func (usr *User) CreateMailbox(name string) (err error) {
	if usr.ExistsMailbox(name) {
		err = ErrMailboxExists
	} else {
		usr.Mailboxes[name] = &Mailbox{Name: name, Owner: usr}
	}
	return
}

func (usr *User) DeleteMailbox(name string) (err error) {
	if usr.isProtected(name) {
		err = ErrDeleteProtect
	} else if !usr.ExistsMailbox(name) {
		err = ErrNoSuchMailbox
	} else {
		delete(usr.Mailboxes, name)
	}
	return
}

func (usr *User) RenameMailbox(existingName, newName string) (err error) {
	if usr.ExistsMailbox(existingName) {
		err = ErrNoSuchMailbox
	} else {

		usr.CreateMailbox(newName)

		newBox, existenceErr := usr.GetMailbox(newName)

		if existenceErr != nil {
			err = ErrCreateMailbox
		} else {
			mBox, _ := usr.GetMailbox(existingName)

			newBox.Messages = mBox.Messages

			mBox.Messages = []*Message{}

			if !usr.isProtected(existingName) {
				usr.DeleteMailbox(existingName)
			}
		}

	}
	return
}

func (usr *User) isProtected(name string) (protected bool) {
	for _, b := range ProtectedMailboxes {
		if b == name {
			protected = true
			break
		}
	}
	return
}