package l

import "log"

func Println(message string) {
	log.Println(message)
}

func Fatal(v ...interface{}) {
	log.Fatal(v)
}
