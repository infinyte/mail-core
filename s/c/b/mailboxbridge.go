package b

import (
	"bitbucket.org/infinyte/mail-core/i"
	"time"
	"github.com/emersion/go-imap"
	"io/ioutil"
	"github.com/emersion/go-imap/backend/backendutil"
)

type MailboxBridge struct {
	theRealSlimMailbox *i.Mailbox
	theRealSlimOwner   *UserBridge
}

// Bridge Functions
func (mBox *MailboxBridge) GetBridgedMailbox() (*i.Mailbox) {
	return mBox.theRealSlimMailbox
}

func (mBox *MailboxBridge) GetOwner() (*UserBridge) {
	return mBox.theRealSlimOwner
}

func (mBox *MailboxBridge) GetMessages() (messages []*MessageBridge) {
	for _, msg := range mBox.GetBridgedMailbox().GetMessages() {
		messages = append(messages, NewMessageBridge(msg))
	}
	return
}

func (mBox *MailboxBridge) Name() string {
	return mBox.GetBridgedMailbox().Name
}

func (mBox *MailboxBridge) Info() (*imap.MailboxInfo, error) {
	info := &imap.MailboxInfo{
		Delimiter: i.Delimiter,
		Name:      mBox.Name(),
	}
	return info, nil
}

func (mBox *MailboxBridge) uidNext() uint32 {
	return mBox.GetBridgedMailbox().GetNextUid()
}

func (mBox *MailboxBridge) flags() []string {
	return mBox.GetBridgedMailbox().GetFlags()
}

// Non-bridgeable
func (mBox *MailboxBridge) unseenSeqNum() uint32 {
	for i, msg := range mBox.GetBridgedMailbox().Messages {
		seqNum := uint32(i + 1)

		seen := false
		for _, flag := range msg.Flags {
			if flag == imap.SeenFlag {
				seen = true
				break
			}
		}

		if !seen {
			return seqNum
		}
	}
	return 0
}

// Non-bridgeable
func (mBox *MailboxBridge) Status(items []imap.StatusItem) (*imap.MailboxStatus, error) {
	status := imap.NewMailboxStatus(mBox.Name(), items)
	status.Flags = mBox.flags()
	status.PermanentFlags = []string{"\\*"}
	status.UnseenSeqNum = mBox.unseenSeqNum()

	for _, name := range items {
		switch name {
		case imap.StatusMessages:
			status.Messages = uint32(len(mBox.GetBridgedMailbox().Messages))
		case imap.StatusUidNext:
			status.UidNext = mBox.uidNext()
		case imap.StatusUidValidity:
			status.UidValidity = 1
		case imap.StatusRecent:
			status.Recent = 0 // TODO
		case imap.StatusUnseen:
			status.Unseen = 0 // TODO
		}
	}

	return status, nil
}

func (mBox *MailboxBridge) SetSubscribed(subscribed bool) error {
	mBox.GetBridgedMailbox().SetSubscribed(subscribed)
	return nil
}

// Dafuck
func (mBox *MailboxBridge) Check() error {
	return nil
}

// Non-bridgeable
func (mBox *MailboxBridge) ListMessages(uid bool, seqSet *imap.SeqSet, items []imap.FetchItem, ch chan<- *imap.Message) error {
	defer close(ch)

	for i, msg := range mBox.GetMessages() {
		seqNum := uint32(i + 1)

		var id uint32
		if uid {
			id = msg.GetBridgedMessage().Uid
		} else {
			id = seqNum
		}
		if !seqSet.Contains(id) {
			continue
		}

		m, err := msg.Fetch(seqNum, items)
		if err != nil {
			continue
		}

		ch <- m
	}

	return nil
}

// Non-bridgeable
func (mBox *MailboxBridge) SearchMessages(uid bool, criteria *imap.SearchCriteria) ([]uint32, error) {
	var ids []uint32
	for i, msg := range mBox.GetMessages() {
		seqNum := uint32(i + 1)

		ok, err := msg.Match(seqNum, criteria)
		if err != nil || !ok {
			continue
		}

		var id uint32
		if uid {
			id = msg.GetBridgedMessage().Uid
		} else {
			id = seqNum
		}
		ids = append(ids, id)
	}
	return ids, nil
}

func (mBox *MailboxBridge) CreateMessage(flags []string, date time.Time, body imap.Literal) error {
	b, err := ioutil.ReadAll(body)
	if err != nil {
		return err
	}

	mBox.GetBridgedMailbox().CreateMessage(flags, date, string(b))
	return nil
}

func (mBox *MailboxBridge) UpdateMessagesFlags(uid bool, seqset *imap.SeqSet, op imap.FlagsOp, flags []string) error {
	for i, msg := range mBox.GetBridgedMailbox().Messages {
		var id uint32
		if uid {
			id = msg.Uid
		} else {
			id = uint32(i + 1)
		}
		if !seqset.Contains(id) {
			continue
		}

		msg.Flags = backendutil.UpdateFlags(msg.Flags, op, flags)
	}

	return nil
}

func (mBox *MailboxBridge) CopyMessages(uid bool, seqset *imap.SeqSet, destName string) error {
	dest, err := mBox.GetOwner().GetBridgedUser().GetMailbox(destName)
	if err != nil {
		return err
	}

	for i, msg := range mBox.GetBridgedMailbox().Messages {
		var id uint32
		if uid {
			id = msg.Uid
		} else {
			id = uint32(i + 1)
		}
		if !seqset.Contains(id) {
			continue
		}

		msgCopy := *msg
		msgCopy.Uid = dest.GetNextUid()
		dest.Messages = append(dest.Messages, &msgCopy)
	}

	return nil
}

func (mBox *MailboxBridge) Expunge() error {
	for i := len(mBox.GetBridgedMailbox().Messages) - 1; i >= 0; i-- {
		msg := mBox.GetBridgedMailbox().Messages[i]

		deleted := false
		for _, flag := range msg.Flags {
			if flag == imap.DeletedFlag {
				deleted = true
				break
			}
		}

		if deleted {
			mBox.GetBridgedMailbox().Messages = append(mBox.GetBridgedMailbox().Messages[:i], mBox.GetBridgedMailbox().Messages[i+1:]...)
		}
	}

	return nil
}

func NewMailboxBridge(mailbox *i.Mailbox, bridge *UserBridge) (mailboxBridge *MailboxBridge) {
	mailboxBridge = new(MailboxBridge)
	mailboxBridge.theRealSlimMailbox = mailbox
	return &MailboxBridge{
		mailbox,
		bridge,
	}
}
