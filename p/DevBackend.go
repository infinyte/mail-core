package p

import (
	"bitbucket.org/infinyte/mail-core/i"
	"bitbucket.org/infinyte/mail-core/s/c"
	"sync"
	"bitbucket.org/infinyte/mail-core/l"
	"log"
	"time"
)

var accounts = map[string]string{
	"anonymous":         "",
	"test@infinyte.net": "gibbiX12345",
}

type DevBackend struct {
	Mutex sync.Mutex

	UserCache map[string]*i.User
}

func (dev *DevBackend) ListenAndServe(channel *chan c.ChannelMessage) {
	for {

		select {
		default:
			msg := <-*channel
			dev.handleChannelMessage(&msg)
		}

	}
}

func (dev *DevBackend) handleChannelMessage(channelMessage *c.ChannelMessage) {
	defer dev.Mutex.Unlock()
	dev.Mutex.Lock()

	switch channelMessage.Identifier {

	case c.MsgUserLookup:
		c.Send(dev.lookupUser(channelMessage))
		break

	case c.MsgAuthenticationAttempt:
		c.Send(dev.authenticateUser(channelMessage))
		break
	default:
		cm := c.NewMessage()
		cm.Identifier = c.MsgResponse
		cm.Uid = channelMessage.Uid
		c.Send(cm)
	}

}

func (dev *DevBackend) lookupUser(channelMessage *c.ChannelMessage) (channelResponse *c.ChannelMessage) {
	username := channelMessage.GetParameter("Username")
	channelResponse = c.NewMessageResponse(channelMessage)

	if username != nil {
		l.Println("Searching user " + username.(string))

		if _, exists := accounts[username.(string)]; exists {
			if user, cached := dev.UserCache[username.(string)]; cached {
				log.Println("Used Cache for " + user.Username)
				channelResponse.SetParameter("User", user)
			} else {
				user := &i.User{
					Authenticated: false,
					Username:      username.(string),
					Mailboxes:     make(map[string]*i.Mailbox),
				}
				user.CreateMailbox("INBOX")

				body := `Date: Sun, 11 Mar 2018 23:23:56 +0100
Subject: Test
From: Michael Teuscher <test@infinyte.net>
To: Michael Teuscher <test@infinyte.net>
Message-ID: <C3FF3E9E-5AA8-4812-9C33-3F9A52342446@infinyte.net>
Thread-Topic: Test
Mime-version: 1.0
Content-type: text/plain;
	charset="UTF-8"
Content-transfer-encoding: 7bit

test

`

				mBox, _ := user.GetMailbox("INBOX")
				mBox.CreateMessage([]string{"\\Seen"}, time.Now(), body)

				dev.UserCache[user.Username] = user
				channelResponse.SetParameter("User", user)
			}
		}
	}

	return
}

func (dev *DevBackend) authenticateUser(channelMessage *c.ChannelMessage) (channelResponse *c.ChannelMessage) {
	user := channelMessage.GetParameter("User")
	password := channelMessage.GetParameter("Password")

	channelResponse = c.NewMessageResponse(channelMessage)

	if user != nil && password != nil {

		iUser := user.(*i.User)

		if iUser.Authenticated {
			channelResponse.SetParameter("LoginState", iUser.Authenticated)
		} else {
			channelResponse.SetParameter("LoginState", password == accounts[iUser.Username])
		}

	}

	return
}

func NewDevBackend() (dev *DevBackend) {
	dev = &DevBackend{
		UserCache: make(map[string]*i.User),
	}
	return
}
