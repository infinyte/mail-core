package b

import (
	"bitbucket.org/infinyte/mail-core/s/c"
	"log"
	"sync"
	"github.com/emersion/go-imap/backend"
	"bitbucket.org/infinyte/mail-core/s/c/b"
	"github.com/emersion/go-smtp"
)

type BackendBridge struct {
	smtp.Backend
	Mutex sync.Mutex
}

func (bck BackendBridge) Login(username, password string) (user smtp.User, err error) {
	defer bck.Mutex.Unlock()
	bck.Mutex.Lock()

	err = backend.ErrInvalidCredentials

	userObject := c.LookupUser(username, 5)

	if userObject != nil {

		loginState := c.AuthenticateUser(userObject, password, 5)

		if loginState {
			log.Println("Login succeeded for " + username)
			bridgedUser := b.NewUserBridge(userObject)
			user = bridgedUser
			err = nil
		} else {
			log.Println("Login failed for " + username)
			err = backend.ErrInvalidCredentials
		}
	}
	return
}

func (bck *BackendBridge) AnonymousLogin() (smtp.User, error) {
	defer bck.Mutex.Unlock()
	bck.Mutex.Lock()

	userObject := c.LookupUser("anonymous", 5)
	if userObject != nil {

		userObject.Anonymous = true
		bridgedUser := b.NewUserBridge(userObject)
		return bridgedUser, nil
	}
	return nil, smtp.ErrAuthRequired
}
