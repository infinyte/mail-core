package b

import (
	"bytes"
	"github.com/emersion/go-message"
	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/backend/backendutil"
	"bitbucket.org/infinyte/mail-core/i"
	"log"
)

type MessageBridge struct {
	theRealSlimMessage *i.Message
}

func (msg *MessageBridge) GetBridgedMessage() (*i.Message) {
	return msg.theRealSlimMessage
}

func (msg *MessageBridge) entity() (*message.Entity, error) {
	byteBody := []byte(msg.GetBridgedMessage().Body)
	byteReader := bytes.NewReader(byteBody)
	return message.Read(byteReader)
}

func (msg *MessageBridge) Fetch(seqNum uint32, items []imap.FetchItem) (*imap.Message, error) {
	fetched := imap.NewMessage(seqNum, items)
	for _, item := range items {
		switch item {
		case imap.FetchEnvelope:
			e, _ := msg.entity()
			fetched.Envelope, _ = backendutil.FetchEnvelope(e.Header)
		case imap.FetchBody, imap.FetchBodyStructure:
			e, _ := msg.entity()
			fetched.BodyStructure, _ = backendutil.FetchBodyStructure(e, item == imap.FetchBodyStructure)
		case imap.FetchFlags:
			fetched.Flags = msg.GetBridgedMessage().Flags
		case imap.FetchInternalDate:
			fetched.InternalDate = msg.GetBridgedMessage().Date
		case imap.FetchRFC822Size:
			fetched.Size = msg.GetBridgedMessage().Size
		case imap.FetchUid:
			fetched.Uid = msg.GetBridgedMessage().Uid
		default:
			section, err := imap.ParseBodySectionName(item)
			if err != nil {
				break
			}

			e, err := msg.entity()

			log.Println(err)

			l, _ := backendutil.FetchBodySection(e, section)
			fetched.Body[section] = l
		}
	}

	return fetched, nil
}

func (msg *MessageBridge) Match(seqNum uint32, c *imap.SearchCriteria) (bool, error) {
	if !backendutil.MatchSeqNumAndUid(seqNum, msg.GetBridgedMessage().Uid, c) {
		return false, nil
	}
	if !backendutil.MatchDate(msg.GetBridgedMessage().Date, c) {
		return false, nil
	}
	if !backendutil.MatchFlags(msg.GetBridgedMessage().Flags, c) {
		return false, nil
	}

	e, _ := msg.entity()
	return backendutil.Match(e, c)
}

func NewMessageBridge(message *i.Message) (messageBridge *MessageBridge) {
	return &MessageBridge{
		message,
	}
}
